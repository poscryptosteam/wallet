#!/bin/bash

#For every change in JSX files you need run this file for avoid errors like:
#does not exist in the haste module map
#or other random errors

rm -rf /tmp/metro-*
rm -rf /tmp/haste-*

#maybe need clean ./node_modules/
#rm -rf node_modules
#npm install

cd android
./gradlew clean