import React, { Component } from 'react';
import { Button, Text, View } from 'react-native';
import { RNCamera } from 'react-native-camera';
import Web3 from 'web3';

const web3 = new Web3(new Web3.providers.WebsocketProvider('wss://ropsten.infura.io/ws/ea2841e43f2c4f3bb00e54892e820864'));

class QrCodeScreen extends Component {

    constructor(props) {
        super(props);
        this.camera = null;
        this.barcodeCodes = [];

        this.state = {
            camera: {
                type: RNCamera.Constants.Type.back,
                flashMode: RNCamera.Constants.FlashMode.auto,
                barcodeFinderVisible: true
            }
        };
    }

    onBarCodeRead(scanResult) {
        if (scanResult.data != null) {
            console.log(scanResult.type);
            console.warn(scanResult.data);
            if (!this.barcodeCodes.includes(scanResult.data)) {
                this.barcodeCodes.push(scanResult.data);
                console.log('onBarCodeRead call');
                this.getERC20TokenBalance(scanResult.data,"0xb7db906ff24c131087dcb5b57c9e8f1d7b271a16")
            }
        }
        return;
    }

    // async takePicture() {
    //     if (this.camera) {
    //         const options = { quality: 0.5, base64: true };
    //         const data = await this.camera.takePictureAsync(options);
    //         console.log(data.uri);
    //     }
    // }
    //
    // pendingView() {
    //     return (
    //         <View
    //             style={{
    //                 flex: 1,
    //                 backgroundColor: 'lightgreen',
    //                 justifyContent: 'center',
    //                 alignItems: 'center',
    //             }}
    //         >
    //             <Text>Waiting</Text>
    //         </View>
    //     );
    // }


    getERC20TokenBalance() {
        // let source = fs.readFileSync('../contracts/MSC.sol', 'utf8');
        // let compiledContract = solc.compile(source, 1);
        // let abi = compiledContract.contracts['MSC'].interface;
        // let bytecode = compiledContract.contracts['MSC'].bytecode;
        // let MyContract = web3.eth.Contract(JSON.parse(abi));
        // var myContractInstance =  MyContract.at('0xb7db906ff24c131087dcb5b57c9e8f1d7b271a16')

        // myContractInstance.transfer("0x88a007ec4f1819f24c0988fc9c26496b99b436d1", "100000000")



        // contractInstance.balanceOf(my_address, function(error, success){
        //     if(error) console.log ("Something went wrong: " + error);
        //     else console.log ("Balance: " + success.toString(10));
        // });
    }


    render() {
        return (
            <View style={styles.container}>
                <RNCamera
                    ref={ref => {
                        this.camera = ref;
                    }}
                    barcodeFinderVisible={this.state.camera.barcodeFinderVisible}
                    barcodeFinderWidth={280}
                    barcodeFinderHeight={220}
                    barcodeFinderBorderColor="white"
                    barcodeFinderBorderWidth={2}
                    defaultTouchToFocus
                    flashMode={this.state.camera.flashMode}
                    mirrorImage={false}
                    onBarCodeRead={this.onBarCodeRead.bind(this)}
                    onFocusChanged={() => {}}
                    onZoomChanged={() => {}}
                    permissionDialogTitle={'Permission to use camera'}
                    permissionDialogMessage={'We need your permission to use your camera phone'}
                    style={styles.preview}
                    type={this.state.camera.type}
                />
                <View style={[styles.overlay, styles.topOverlay]}>
                    <Text style={styles.scanScreenMessage}>Please scan the barcode.</Text>
                </View>
            </View>
        );
    }
}

const styles = {
    container: {
        flex: 1
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    overlay: {
        position: 'absolute',
        padding: 16,
        right: 0,
        left: 0,
        alignItems: 'center'
    },
    topOverlay: {
        top: 0,
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    bottomOverlay: {
        bottom: 0,
        backgroundColor: 'rgba(0,0,0,0.4)',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    enterBarcodeManualButton: {
        padding: 15,
        backgroundColor: 'white',
        borderRadius: 40
    },
    scanScreenMessage: {
        fontSize: 14,
        color: 'white',
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center'
    }
};

export default QrCodeScreen;