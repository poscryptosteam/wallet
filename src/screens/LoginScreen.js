import React from "react";
import { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    Button
} from "react-native";

class LoginScreen extends Component {

    static navigationOptions = {
        header: null
    }

    render() {
        return (
            <View style={styles.container}>
            <Text style={styles.instructions}>
            Do you have wallet?
                </Text>
                <Button title="Import wallet"
                    onPress={() => this.props.navigation.navigate('Home')} />
                    <Button title="Create new wallet"
                    onPress={() => this.props.navigation.navigate('Home')} />
                    
            </View>
        );
    }
}
export default LoginScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5
    }
});