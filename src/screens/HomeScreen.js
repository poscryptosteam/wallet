import React from "react";
import { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    Button
} from "react-native";
import Web3 from 'web3';


const web3 = new Web3(new Web3.providers.WebsocketProvider('wss://ropsten.infura.io/ws/ea2841e43f2c4f3bb00e54892e820864'));

class HomeScreen extends Component {

    // static navigationOptions = {
    //     header: null
    // }

    state = {
        balance: null,
        block: "Wait for block number...",
    };



    getLastBlock() {
        return web3.eth.getBlock('latest').then((block) => {
            console.log(block.number);
            this.setState({block: block.number})
        });
    };

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.instructions}>
                    HomeScreen
                </Text>
                <Button title="QrCode"
                        onPress={() => this.props.navigation.navigate('QrCode')} />

                <Button  onPress={this.getLastBlock.bind(this)} title={"Last Block"}/>

                <Text>
                    {this.state.block}
                </Text>

            </View>
        );
    }
}
export default HomeScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5
    }
});